FROM node:16.10.0-alpine AS build

ENV CI=true

COPY package.json \
     package-lock.json \
     /app/

WORKDIR /app

RUN npm ci --no-progress --color=false --quiet

COPY . /app

RUN npm run build

# Copy docs to alpine-based nginx container.
FROM nginx:alpine
EXPOSE 8080

RUN adduser -D -u 1001 appuser

RUN touch /var/run/nginx.pid && \
    chown -R appuser /var/run/nginx.pid && \
    chown -R appuser /var/cache/nginx

COPY ./docker/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/default.conf /etc/nginx/conf.d/default.conf

COPY --from=build /app/dist /usr/share/nginx/html

USER appuser
